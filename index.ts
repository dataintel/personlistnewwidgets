import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetListPersonComponent } from './src/listperson.component';

export * from './src/listperson.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetListPersonComponent
  ],
  exports: [
    BaseWidgetListPersonComponent
  ]
})
export class ListPersonModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ListPersonModule,
      providers: []
    };
  }
}
