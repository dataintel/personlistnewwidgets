import { Component } from '@angular/core';

@Component({
  selector: 'list-person-component',
  template: `<div  id="list-catalog" class="content list-group catalog-group">
    <div class="col-md-12" *ngFor="let account of listperson; let ind = index">
        <div id="{{account.id}}" class="list-group-item" title="{{account.name}} " (click)="onSelect(ind)" [ngClass]="{active: (selectedIndex === ind)}">
            <span class="img" [ngStyle]="{'background-image': 'url(' + account.image + '),url(assets/img/nouser.png)'}"></span>
            <p>
                <b>{{account.name}}</b><br>
                <small>{{account.count}} statements</small>
            </p>
        </div>
    </div>
</div>
<div class="col-md-12">    
    <label class="sumrows-catalog">{{totalItems}} Persons<span id="info-selected-catalog"></span></label>    

</div>
<style>
#list-catalog {
    margin-top: 10px;
    margin-bottom: 10px; 
    display: table;
 
}
#list-catalog > div {
    padding-left: 5px; 
    padding-right: 5px;
}
#list-catalog .list-group-item {
    border: none;
    display: table;
    height: 45px;
    padding: 0 10px 0 0;
}

#list-catalog .list-group-item > .img {
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    cursor: pointer;
    float: left;
    height: 35px;
    margin: 5px 10px 5px 5px;
    width: 35px;
}
#list-catalog .list-group-item > p {
    display: table-cell;
    margin: 0;
    padding: 0;
    vertical-align: middle;
    width: 100%;
    cursor: pointer;
    line-height: 1;
}

.pagination {
    margin: 0;
}

.sumrows-catalog{
    margin-top: 10px;
}
</style>`
})
export class BaseWidgetListPersonComponent {
public listperson = [
      {
        "count": 1858,
        "id": "najib+razak",
        "name": "Najib Razak",
        "image": "/assets/nouser.png"
      },
      {
        "count": 392,
        "id": "mahathir+mohamad",
        "name": "Mahathir Mohamad",
        "image": "/assets/nouser.png"
      },
      {
        "count": 195,
        "id": "kim+jong+nam",
        "name": "Kim Jong Nam",
        "image": "/assets/nouser.png"
      },
      {
        "count": 169,
        "id": "najib",
        "name": "Najib",
        "image": "/assets/nouser.png"
      },
      {
        "count": 147,
        "id": "kang+chol",
        "name": "Kang Chol",
        "image": "/assets/nouser.png"
      },
      {
        "count": 117,
        "id": "ahmad+zahid+hamidi",
        "name": "Ahmad Zahid Hamidi",
        "image": "/assets/nouser.png"
      },
      {
        "count": 109,
        "id": "lim+kit+siang",
        "name": "Lim Kit Siang",
        "image": "/assets/nouser.png"
      },
      {
        "count": 105,
        "id": "kim+jong+un",
        "name": "Kim Jong Un",
        "image": "/assets/nouser.png"
      },
      {
        "count": 83,
        "id": "sultan+muhammad+v",
        "name": "Sultan Muhammad V",
        "image": "/assets/nouser.png"
      },
      {
        "count": 83,
        "id": "abdul+hadi+awang",
        "name": "Abdul Hadi Awang",
        "image": "/assets/nouser.png"
      }
    ];
  constructor() {
  }

}
